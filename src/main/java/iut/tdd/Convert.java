package iut.tdd;

public class Convert {

    public static String num2text(String input) {
        if (input.charAt(0) == '0') {
            return "zéro";
        }     
        if (input.charAt(0) == '1') {
            return "un";
        }
        if (input.charAt(0) == '2') {
            return "deux";
        }
        if (input.charAt(0) == '3') {
            return "trois";
        }
        if (input.charAt(0) == '4') {
            return "quatre";
        }
        if (input.charAt(0) == '5') {
            return "cinq";
        }
        if (input.charAt(0) == '6') {
            return "six";
        }
        if (input.charAt(0) == '7') {
            return "sept";
        }
        if (input.charAt(0) == '8') {
            return "huit";
        }       
        if (input.charAt(0) == '9') {
            return "neuf";
        }               
        
        return null;
    }

    public static String text2num(String input) {
        return null;
    }
}
